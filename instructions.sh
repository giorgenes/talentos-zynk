#!/bin/sh
URL="http://talentos-zynk.rhcloud.com/contact.json/"
HEADER="Accept-Encoding: application/json"
DECODE_PROGRAM="base64 -D"

# Get the data.
data=$(curl -H "$HEADER" -s $URL)

# Get the subject.
subject=$(echo "$data" | python -m json.tool | awk '/withTheSubject/ { print $2 }' | tr -d \")

# Decode the subject.
echo "================================================="
echo "HOW TO APPLY"
echo $data
echo 

if [ -n "$subject" ]; then
	decodedSubject=$(echo "$subject" | $DECODE_PROGRAM)
	echo "================================================="
	echo "SUBJECT"
	if [ -n "$decodedSubject" ]; then
		echo $decodedSubject
	else
		echo "Decode this to find out the subject: "$subject
	fi
	echo "================================================="
fi
